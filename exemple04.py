import pygame

'''
En aquest exemple:

- Organització del programa en funcions.
- Control de la velocitat de l'animació.
'''

def move_player(player, delta):
    moved=False
    vel=int(0.2*delta)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player['x']-=vel
        player['sprites']=player['left']
        moved=True
    if keys[pygame.K_RIGHT]:
        player['x']+=vel
        player['sprites']=player['right']
        moved=True
    if keys[pygame.K_UP]:
        player['y']-=vel
        player['sprites']=player['up']
        moved=True
    if keys[pygame.K_DOWN]:
        player['y']+=vel
        player['sprites']=player['down']
        moved=True
    if moved:
        player['animation_time']+=delta
        if player['animation_time']>400:
            player['animation_time']-=400
            player['sprite_index']=(player['sprite_index']+1)%4

def draw_player(screen, player):
    sprite=player['sprites'][player['sprite_index']]
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)

def create_player():
    player={
        'right': [
            pygame.image.load('images/pig/right1.png'),
            pygame.image.load('images/pig/right2.png'),
            pygame.image.load('images/pig/right3.png'),
            pygame.image.load('images/pig/right4.png')
        ],
        'left': [
            pygame.image.load('images/pig/left1.png'),
            pygame.image.load('images/pig/left2.png'),
            pygame.image.load('images/pig/left3.png'),
            pygame.image.load('images/pig/left4.png')
        ],
        'up': [
            pygame.image.load('images/pig/up1.png'),
            pygame.image.load('images/pig/up2.png'),
            pygame.image.load('images/pig/up3.png'),
            pygame.image.load('images/pig/up4.png')
        ],
        'down': [
            pygame.image.load('images/pig/down1.png'),
            pygame.image.load('images/pig/down2.png'),
            pygame.image.load('images/pig/down3.png'),
            pygame.image.load('images/pig/down4.png')
        ],
        'sprite_index': 0,
        'animation_time': 0,
        'x':400-27,
        'y':300-15
    }
    player['sprites']=player['right']
    return player

pygame.init()
screen = pygame.display.set_mode([800, 600])
game_icon = pygame.image.load('images/icon.png')
player=create_player()

pygame.display.set_caption('Exemple 4')
pygame.display.set_icon(game_icon)

clock = pygame.time.Clock()
going=True
while going:
    delta=clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            going=False
    screen.fill((128,128,128))
    move_player(player, delta)
    draw_player(screen, player)
    pygame.display.flip()
pygame.quit()
