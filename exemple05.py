import pygame

FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_VEL=0.2

'''
En aquest exemple:

- Evitem l'ús de variables globals posant el programa principal dins d'una funció.
- Comencem a utilitzar constants per guardar dades fixes del programa.
- Impedim que el personatge pugui moure's fora de la pantalla.
'''

def move_player(player, delta):
    moved=False
    vel=int(PLAYER_VEL*delta)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and player['x']>0:
        player['x']=max(player['x']-vel, 0)
        player['sprites']=player['left']
        moved=True
        player['animation_time']
    if keys[pygame.K_RIGHT] and player['x']<WINDOW_SIZE_X-25:
        player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
        player['sprites']=player['right']
        moved=True
    if keys[pygame.K_UP] and player['y']>0:
        player['y']=max(player['y']-vel, 0)
        player['sprites']=player['up']
        moved=True
    if keys[pygame.K_DOWN] and player['y']<WINDOW_SIZE_Y-25:
        player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
        player['sprites']=player['down']
        moved=True
    if moved:
        player['animation_time']+=delta
        if player['animation_time']>400:
            player['animation_time']-=400
            player['sprite_index']=(player['sprite_index']+1)%4

def draw_player(screen, player):
    sprite=player['sprites'][player['sprite_index']]
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)

def create_player():
    player={
        'right': [
            pygame.image.load('images/pig/right1.png'),
            pygame.image.load('images/pig/right2.png'),
            pygame.image.load('images/pig/right3.png'),
            pygame.image.load('images/pig/right4.png')
        ],
        'left': [
            pygame.image.load('images/pig/left1.png'),
            pygame.image.load('images/pig/left2.png'),
            pygame.image.load('images/pig/left3.png'),
            pygame.image.load('images/pig/left4.png')
        ],
        'up': [
            pygame.image.load('images/pig/up1.png'),
            pygame.image.load('images/pig/up2.png'),
            pygame.image.load('images/pig/up3.png'),
            pygame.image.load('images/pig/up4.png')
        ],
        'down': [
            pygame.image.load('images/pig/down1.png'),
            pygame.image.load('images/pig/down2.png'),
            pygame.image.load('images/pig/down3.png'),
            pygame.image.load('images/pig/down4.png')
        ],
        'sprite_index': 0,
        'animation_time': 0,
        'x':400-27,
        'y':300-15
    }
    player['sprites']=player['right']
    return player

def main():
    pygame.init()
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    game_icon = pygame.image.load('images/icon.png')
    player=create_player()

    pygame.display.set_caption('Exemple 5')
    pygame.display.set_icon(game_icon)

    clock = pygame.time.Clock()
    going=True
    while going:
        delta=clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
        screen.fill((128,128,128))
        move_player(player, delta)
        draw_player(screen, player)
        pygame.display.flip()
    pygame.quit()

main()
