import pygame

'''
En aquest exemple:

- Comprovació de tecles premudes
- Moure un sprite per pantalla
- Ús del temps transcorregut entre frame i frame (delta) per calcular la nova posició
'''

pygame.init()
screen = pygame.display.set_mode([800, 600])
game_icon = pygame.image.load('images/icon.png')
pig = pygame.image.load('images/pig/right1.png')
x=400-27
y=300-15
pygame.display.set_caption('Exemple 2')
pygame.display.set_icon(game_icon)

clock = pygame.time.Clock()
going=True
while going:
    delta=clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            going=False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        x-=int(0.2*delta)
    if keys[pygame.K_RIGHT]:
        x+=int(0.2*delta)
    screen.fill((128,128,128))
    square=pig.get_rect().move(x, y)
    screen.blit(pig, square)
    pygame.display.flip()
pygame.quit()