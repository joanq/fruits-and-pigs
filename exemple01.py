import pygame

'''
En aquest exemple:

- Esquema bàsic d'una aplicació amb pygame
- Carregar imatges
- Assignar frames per segon (FPS)
- Dibuixar un sprite a pantalla
'''

pygame.init()
screen = pygame.display.set_mode([800, 600])
game_icon = pygame.image.load('images/icon.png')
pig = pygame.image.load('images/pig/right1.png')
pygame.display.set_caption('Exemple 1')
pygame.display.set_icon(game_icon)

clock = pygame.time.Clock()
going=True
while going:
    delta=clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            going=False
    
    screen.fill((128,128,128))
    square=pig.get_rect().move(400-27, 300-15)
    screen.blit(pig, square)
    pygame.display.flip()
pygame.quit()