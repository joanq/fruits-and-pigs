import pygame

'''
En aquest exemple:

- Ús de diccionaris per agrupar les dades relacionades a un objecte del joc.
- Animació del protagonista.
'''

pygame.init()
screen = pygame.display.set_mode([800, 600])
game_icon = pygame.image.load('images/icon.png')

player={
    'right': [
        pygame.image.load('images/pig/right1.png'),
        pygame.image.load('images/pig/right2.png'),
        pygame.image.load('images/pig/right3.png'),
        pygame.image.load('images/pig/right4.png')
    ],
    'left': [
        pygame.image.load('images/pig/left1.png'),
        pygame.image.load('images/pig/left2.png'),
        pygame.image.load('images/pig/left3.png'),
        pygame.image.load('images/pig/left4.png')
    ],
    'up': [
        pygame.image.load('images/pig/up1.png'),
        pygame.image.load('images/pig/up2.png'),
        pygame.image.load('images/pig/up3.png'),
        pygame.image.load('images/pig/up4.png')
    ],
    'down': [
        pygame.image.load('images/pig/down1.png'),
        pygame.image.load('images/pig/down2.png'),
        pygame.image.load('images/pig/down3.png'),
        pygame.image.load('images/pig/down4.png')
    ],
    'sprite_index': 0,
    'x':400-27,
    'y':300-15
}
player['sprites']=player['right']

pygame.display.set_caption('Exemple 3')
pygame.display.set_icon(game_icon)

clock = pygame.time.Clock()
going=True
while going:
    delta=clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            going=False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player['x']-=int(0.2*delta)
        player['sprites']=player['left']
        player['sprite_index']=(player['sprite_index']+1)%4
    if keys[pygame.K_RIGHT]:
        player['x']+=int(0.2*delta)
        player['sprites']=player['right']
        player['sprite_index']=(player['sprite_index']+1)%4
    screen.fill((128,128,128))
    sprite=player['sprites'][player['sprite_index']]
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)
    pygame.display.flip()
pygame.quit()