import pygame
import pygame.freetype
import random

FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_VEL=0.2
SPAWN_FRUIT_TIME=500

# game states
GAME_STATE_MENU=1
GAME_STATE_PLAYING=2
GAME_STATE_RESULTS=3
GAME_STATE_EXIT=4

'''
En aquest exemple:

- Menú d'inici
- Estats del joc
- Posició del ratolí
- Clic del ratolí
- Detalls: temps de les animacions
'''

def eat_fruit(player, fruits):
    sprite=player['sprites'][player['sprite_index']]
    square=sprite.get_rect().move(player['x'], player['y'])
    for fruit in fruits:
        fruit_square=fruit['sprite'].get_rect().move(fruit['x'], fruit['y'])
        if square.colliderect(fruit_square):
            player['points']+=fruit['points']
            fruit['time']=0
            player['action']='eat'
            player['sprite_index']=0
            player['sprites']=player['eat_'+player['facing']]

def spawn_fruit(fruit_types):
    fruit_type=random.choice(fruit_types)
    fruit={
        'sprite': fruit_type['sprite'],
        'time': fruit_type['time'],
        'points': fruit_type['points'],
        'x': random.randint(0, WINDOW_SIZE_X-fruit_type['sprite'].get_width()),
        'y': random.randint(0, WINDOW_SIZE_Y-fruit_type['sprite'].get_height())
    }
    return fruit

def fruits_decay(fruits, delta):
    for n in range(len(fruits)-1, -1, -1):
        fruit=fruits[n]
        fruit['time']-=delta

        if fruit['time']<=0:
            del fruits[n]
    
def move_player(player, delta):
    moved=False
    animation_time=200
    vel=int(PLAYER_VEL*delta)
    keys = pygame.key.get_pressed()
    if player['action']=='walk':
        if keys[player['keys']['left']] and player['x']>0:
            player['x']=max(player['x']-vel, 0)
            player['facing']='left'
            player['sprites']=player['left']
            moved=True
            player['animation_time']
        if keys[player['keys']['right']] and player['x']<WINDOW_SIZE_X-25:
            player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
            player['facing']='right'
            player['sprites']=player['right']
            moved=True
        if keys[player['keys']['up']] and player['y']>0:
            player['y']=max(player['y']-vel, 0)
            player['facing']='up'
            player['sprites']=player['up']
            moved=True
        if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25:
            player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
            player['facing']='down'
            player['sprites']=player['down']
            moved=True
    elif player['action']=='eat':
        moved=True
        animation_time=100
    if moved:
        player['animation_time']+=delta
        if player['animation_time']>animation_time:
            player['animation_time']-=animation_time
            player['sprite_index']=(player['sprite_index']+1)%4
            if player['sprite_index']==0 and player['action']=='eat':
                player['action']='walk'
                player['sprites']=player[player['facing']]

def draw_player(screen, players):
    for player in players:
        sprite=player['sprites'][player['sprite_index']]
        square=sprite.get_rect().move(player['x'], player['y'])
        screen.blit(sprite, square)

def draw_fruits(screen, fruits):
    for fruit in fruits:
        sprite=fruit['sprite']
        square=sprite.get_rect().move(fruit['x'], fruit['y'])
        screen.blit(sprite, square)

def create_fruit_types():
    fruit_types=[
        {'sprite': pygame.image.load('images/fruits/fruit1.png'),
         'time': 10000,
         'points': 10},
        {'sprite': pygame.image.load('images/fruits/fruit2.png'),
         'time': 10000,
         'points': 8},
        {'sprite': pygame.image.load('images/fruits/fruit3.png'),
         'time': 20000,
         'points': 6},
        {'sprite': pygame.image.load('images/fruits/fruit4.png'),
         'time': 12000,
         'points': 10},
        {'sprite': pygame.image.load('images/fruits/fruit10.png'),
         'time': 6000,
         'points': 20},
        {'sprite': pygame.image.load('images/fruits/fruit13.png'),
         'time': 14000,
         'points': 20}
    ]
    return fruit_types

def create_players():
    prefix=['', 'eat_']
    sufix=['', 'dark']
    dirs=['right', 'left', 'up', 'down']
    facing=['right', 'left']
    keys=[
        {
            'right': pygame.K_RIGHT,
            'left': pygame.K_LEFT,
            'up': pygame.K_UP,
            'down': pygame.K_DOWN
        },
        {
            'right': pygame.K_d,
            'left': pygame.K_a,
            'up': pygame.K_w,
            'down': pygame.K_s
        }
    ]
    x=[500,300]
    players=[]
    for n_player in range(0,2):
        player={
            'sprite_index': 0,
            'animation_time': 0,
            'x':x[n_player]-27,
            'y':300-15,
            'points':0,
            'action':'walk',
            'facing':facing[n_player],
            'keys': keys[n_player]
        }
        for pre in prefix:
            for dir in dirs:
                l=[]
                for n in range(1,5):
                    l.append(pygame.image.load('images/pig/'+pre+dir+str(n)+sufix[n_player]+'.png'))
                player[pre+dir]=l
        player['sprites']=player[facing[n_player]]
        players.append(player)
    return players

def draw_overlay(screen, font, players):
    font.render_to(screen, (550, 550), 'Puntuació: '+str(players[0]['points']))
    font.render_to(screen, (50, 550), 'Puntuació: '+str(players[1]['points']))

def game_playing(screen):
    overlay_font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    players=create_players()
    fruit_types=create_fruit_types()
    fruits=[]
    clock = pygame.time.Clock()
    elapsed_time=0
    going=True
    while going:
        delta=clock.tick(FPS)
        elapsed_time+=delta
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
        screen.fill((128,128,128))
        if elapsed_time>SPAWN_FRUIT_TIME:
            elapsed_time-=SPAWN_FRUIT_TIME
            fruits.append(spawn_fruit(fruit_types))
        for player in players:
            move_player(player, delta)
            eat_fruit(player, fruits)
        fruits_decay(fruits, delta)
        draw_fruits(screen, fruits)
        draw_player(screen, players)
        draw_overlay(screen, overlay_font, players)
        pygame.display.flip()
    return result

def game_menu(screen):
    title=pygame.image.load('images/menu/title.png')
    start_btn_light=pygame.image.load('images/menu/start_button.png')
    start_btn_dark=pygame.image.load('images/menu/start_buttondark.png')
    exit_btn_light=pygame.image.load('images/menu/exit_button.png')
    exit_btn_dark=pygame.image.load('images/menu/exit_buttondark.png')
    start_btn=start_btn_light
    exit_btn=exit_btn_light
    background=pygame.image.load('images/menu/background.jpg')
    going=True
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=start_btn.get_rect().move(150, 500)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_PLAYING
                square=exit_btn.get_rect().move(450, 500)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_EXIT
        square=start_btn.get_rect().move(150, 500)
        if square.collidepoint(pygame.mouse.get_pos()):
            start_btn=start_btn_dark
        else:
            start_btn=start_btn_light
        square=exit_btn.get_rect().move(450, 500)
        if square.collidepoint(pygame.mouse.get_pos()):
            exit_btn=exit_btn_dark
        else:
            exit_btn=exit_btn_light
        screen.blit(background, background.get_rect())
        screen.blit(title, title.get_rect().move(400-352, 50))
        screen.blit(start_btn, start_btn.get_rect().move(150,500))
        screen.blit(exit_btn, exit_btn.get_rect().move(450,500))
        pygame.display.flip()
    return result

def main():
    pygame.init()
    
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    game_icon = pygame.image.load('images/icon.png')
    
    pygame.display.set_caption('Exemple 10')
    pygame.display.set_icon(game_icon)

    game_state=GAME_STATE_MENU
    while game_state!=GAME_STATE_EXIT:
        if game_state==GAME_STATE_MENU:
            game_state=game_menu(screen)
        elif game_state==GAME_STATE_PLAYING:
            game_state=game_playing(screen)
    pygame.quit()

main()
