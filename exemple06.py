import pygame
import random

FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_VEL=0.2
SPAWN_FRUIT_TIME=2000

'''
En aquest exemple:

- Ús de llistes per guardar múltiples objectes del mateix tipus.
- Esdeveniments que es produeixen cada cert temps.
- Creació i eliminació d'objectes.
'''

def spawn_fruit(fruit_types):
    fruit_type=random.choice(fruit_types)
    fruit={
        'sprite': fruit_type['sprite'],
        'time': fruit_type['time'],
        'points': fruit_type['points'],
        'x': random.randint(0, WINDOW_SIZE_X-fruit_type['sprite'].get_width()),
        'y': random.randint(0, WINDOW_SIZE_Y-fruit_type['sprite'].get_height())
    }
    return fruit

def fruits_decay(fruits, delta):
    for n in range(len(fruits)-1, -1, -1):
        fruit=fruits[n]
        fruit['time']-=delta

        if fruit['time']<=0:
            del fruits[n]
    
def move_player(player, delta):
    moved=False
    vel=int(PLAYER_VEL*delta)
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and player['x']>0:
        player['x']=max(player['x']-vel, 0)
        player['sprites']=player['left']
        moved=True
        player['animation_time']
    if keys[pygame.K_RIGHT] and player['x']<WINDOW_SIZE_X-25:
        player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
        player['sprites']=player['right']
        moved=True
    if keys[pygame.K_UP] and player['y']>0:
        player['y']=max(player['y']-vel, 0)
        player['sprites']=player['up']
        moved=True
    if keys[pygame.K_DOWN] and player['y']<WINDOW_SIZE_Y-25:
        player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
        player['sprites']=player['down']
        moved=True
    if moved:
        player['animation_time']+=delta
        if player['animation_time']>400:
            player['animation_time']-=400
            player['sprite_index']=(player['sprite_index']+1)%4

def draw_player(screen, player):
    sprite=player['sprites'][player['sprite_index']]
    square=sprite.get_rect().move(player['x'], player['y'])
    screen.blit(sprite, square)

def draw_fruits(screen, fruits):
    for fruit in fruits:
        sprite=fruit['sprite']
        square=sprite.get_rect().move(fruit['x'], fruit['y'])
        screen.blit(sprite, square)

def create_fruit_types():
    fruit_types=[
        {'sprite': pygame.image.load('images/fruits/fruit1.png'),
         'time': 10000,
         'points': 10},
        {'sprite': pygame.image.load('images/fruits/fruit2.png'),
         'time': 10000,
         'points': 8},
        {'sprite': pygame.image.load('images/fruits/fruit3.png'),
         'time': 20000,
         'points': 6},
        {'sprite': pygame.image.load('images/fruits/fruit4.png'),
         'time': 12000,
         'points': 10},
        {'sprite': pygame.image.load('images/fruits/fruit10.png'),
         'time': 6000,
         'points': 20},
        {'sprite': pygame.image.load('images/fruits/fruit13.png'),
         'time': 14000,
         'points': 20}
    ]
    return fruit_types

def create_player():
    player={
        'right': [
            pygame.image.load('images/pig/right1.png'),
            pygame.image.load('images/pig/right2.png'),
            pygame.image.load('images/pig/right3.png'),
            pygame.image.load('images/pig/right4.png')
        ],
        'left': [
            pygame.image.load('images/pig/left1.png'),
            pygame.image.load('images/pig/left2.png'),
            pygame.image.load('images/pig/left3.png'),
            pygame.image.load('images/pig/left4.png')
        ],
        'up': [
            pygame.image.load('images/pig/up1.png'),
            pygame.image.load('images/pig/up2.png'),
            pygame.image.load('images/pig/up3.png'),
            pygame.image.load('images/pig/up4.png')
        ],
        'down': [
            pygame.image.load('images/pig/down1.png'),
            pygame.image.load('images/pig/down2.png'),
            pygame.image.load('images/pig/down3.png'),
            pygame.image.load('images/pig/down4.png')
        ],
        'sprite_index': 0,
        'animation_time': 0,
        'x':400-27,
        'y':300-15
    }
    player['sprites']=player['right']
    return player

def main():
    pygame.init()
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    game_icon = pygame.image.load('images/icon.png')
    player=create_player()
    fruit_types=create_fruit_types()
    fruits=[]

    pygame.display.set_caption('Exemple 6')
    pygame.display.set_icon(game_icon)

    clock = pygame.time.Clock()
    elapsed_time=0
    going=True
    while going:
        delta=clock.tick(FPS)
        elapsed_time+=delta
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
        screen.fill((128,128,128))
        if elapsed_time>SPAWN_FRUIT_TIME:
            elapsed_time-=SPAWN_FRUIT_TIME
            fruits.append(spawn_fruit(fruit_types))
        move_player(player, delta)
        fruits_decay(fruits, delta)
        draw_fruits(screen, fruits)
        draw_player(screen, player)
        pygame.display.flip()
    pygame.quit()

main()
